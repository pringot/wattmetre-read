#define _POSIX_C_SOURCE 199309L
#define _DEFAULT_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <time.h>
#include <math.h>

#define BAUD B500000


int read_device_values(int fd, int plug_count)
{
  int ret;

  int frame_size = 11+plug_count*2;

  unsigned char buf;
  unsigned char frame_buf[frame_size];
  int frame_idx;
  int last_frame_err;

  unsigned int frame_escape_flag;
  int16_t frame_plug_value;

  struct timespec frame_time;
  double power;

  frame_idx = -1;
  frame_escape_flag = 0;

  last_frame_err = 0;

  // Print CSV headers
  #ifdef DEBUG
  printf("#raw_fame,#decoded_frame,");
  #endif
  printf("#timestamp,#frame_is_ok");
  for (int i=0; i<plug_count; i++) printf(",#power_plug_%d", i+1);
  printf("\n");

  // Read tty device
  while (ret = read(fd, &buf, 1), ret > 0) {
    #ifdef DEBUG
    // Dump frame content coming from device
    printf("%02x:", buf);
    #endif

    // Frame start
    if (frame_idx == -1 && buf == 0xC0) {

      frame_idx = 0;

      ret = clock_gettime(CLOCK_REALTIME, &frame_time);
      if (ret) {
        perror("error when calling gettime");
      }
    }

    // Frame end
    else if (frame_idx >=0 && buf == 0xC1) {

      #ifdef DEBUG
      // Dump frame content after escaped data is processed
      printf(",");
      for (int i=0; i<frame_size; i++) {
        printf("%02x:", frame_buf[i]);
      }
      printf(",");
      #endif

      printf("%lld.%.9ld", (long long)frame_time.tv_sec, frame_time.tv_nsec);

      if (frame_idx != frame_size) {
        fprintf(stderr, "wrong frame size detected (frame_idx=%d, frame_size=%d)!\n", frame_idx, frame_size);
        last_frame_err = 1;
        printf(",ERR");
        for (int i=0; i<plug_count; i++) printf(",");

      } else {
        //Workaround for "successor of error frame values cannot be trusted"
        if (last_frame_err) {
          printf(",ERR");
        } else {
          printf(",OK");
        }

        // Computing plugs power values from frame content
        for (int i=0; i<plug_count; i++) {

          frame_plug_value = (frame_buf[9+2*i+1] << 8) | frame_buf[9+2*i];

          //Workaround for "-3200W" bug
          if (frame_plug_value == -32768) {
            printf(",");

          } else {

            power = (double)frame_plug_value * 25 / 256;

            //Workaround for "use absolute value if phase and neutral are inverted"
            power = fabs(power);

            //Rounded to 0.1 has wattmetre does not have more precision
            power = round(power*10)/10;
            printf(",%.1f", power);
          }
        }

        last_frame_err = 0;
      }

      printf("\n");
      frame_idx = -1;
  }

    // Special frame byte to advertise the successing byte must be escaped
    else if (frame_idx >=0 && buf == 0x7D) {
      frame_escape_flag = 1;
    }

    // Frame byte content
    else if (frame_idx >= 0 && frame_idx < frame_size) {

      frame_buf[frame_idx] = buf;

      if (frame_escape_flag) {
        frame_buf[frame_idx] &= 0x20;
        frame_escape_flag = 0;
      }
      frame_idx += 1;
    }
    else if (frame_idx >= frame_size) {
      fprintf(stderr, "giant frame detected (frame_idx=%d, frame_size=%d)!\n", frame_idx, frame_size);
      last_frame_err = 1;
      printf(",ERR");
      for (int i=0; i<plug_count; i++) printf(",");
      printf("\n");
      frame_idx = -1;
    }
  }

  if (ret == -1) {
    perror("error while reading");
    exit(1);
  }
  close(fd);
  return 0;
}


int configure_tty(char * devtty, unsigned int baud)
{
    int fd, ret;
    speed_t speed = baud;
    struct termios options;

    if ((fd = open(devtty, O_RDWR|O_NOCTTY)) == -1) {
        perror("Error while openning TTY device");
        exit(1);
    }

    //fcntl(fd, F_SETFL, 0);
    if ((ret = tcgetattr(fd, &options)) != 0) {
        perror("Error while getting terminal's attributes");
        exit(1);
    }
    if ((ret = cfsetispeed(&options, speed)) == -1) {
        perror("Error while setting input speed");
        exit(1);
    }
    if ((ret = cfsetospeed(&options, speed)) == -1) {
        perror("Error while setting output speed");
        exit(1);
    }
    cfmakeraw(&options);
    options.c_cflag |= (CLOCAL | CREAD);
    options.c_cflag &= ~CRTSCTS;
    if (tcsetattr(fd, TCSANOW, &options) != 0) {
        perror("Error while setting terminal's attributes");
        exit(1);
    }
    return fd;
}


#define CONFIG_FRAME_SIZE 14
void send_configuration_frame(int fd, int polling_period)
{
  int ret;
  unsigned char *config_frame;

  unsigned char config_frames[7][CONFIG_FRAME_SIZE] = {
    {0xC0,0x00,0xFC,0x00,0x01,0x07,0x01,0x00,0x00,0x05,0x02,0xA4,0xDC,0xC1},
    {0xC0,0x00,0xFC,0x00,0x01,0x07,0x01,0x00,0x00,0x05,0x04,0x92,0xB9,0xC1},
    {0xC0,0x00,0xFC,0x00,0x01,0x07,0x01,0x00,0x00,0x05,0x0A,0xEC,0x50,0xC1},
    {0xC0,0x00,0xFC,0x00,0x01,0x07,0x01,0x00,0x00,0x05,0x14,0x13,0xA9,0xC1},
    {0xC0,0x00,0xFC,0x00,0x01,0x07,0x01,0x00,0x00,0x05,0x19,0xF6,0x72,0xC1},
    {0xC0,0x00,0xFC,0x00,0x01,0x07,0x01,0x00,0x00,0x05,0x32,0x27,0xED,0xC1},
    {0xC0,0x00,0xFC,0x00,0x01,0x07,0x01,0x00,0x00,0x05,0x64,0x94,0xDA,0xC1},
  };
  switch(polling_period) {
    case 20: config_frame = config_frames[0]; break;
    case 40: config_frame = config_frames[1]; break;
    case 100: config_frame = config_frames[2]; break;
    case 200: config_frame = config_frames[3]; break;
    case 250: config_frame = config_frames[4]; break;
    case 400: config_frame = config_frames[5]; break;
    case 1000: config_frame = config_frames[6]; break;
    default:
      fprintf(stderr, "Cannot configure wattmetre to %d ms polling period\n", polling_period);
      exit(1);
  }
  if ((ret = write(fd, config_frame, CONFIG_FRAME_SIZE)) != CONFIG_FRAME_SIZE) {
    perror("Error while send configuration frame to wattmetre");
    exit(1);
  }
}


int main(int argc, char * argv[])
{
  int fd;
  char *end;
  int plug_count = 42;
  int polling_period = 100;

  if (argc <= 1) {
    fprintf(stderr, "Usage: %s [tty-device] [plug-count] [polling-period]\n", argv[0]);
    exit(1);
  }

  fd = configure_tty(argv[1], BAUD);

  if (argc > 2) {
    plug_count = strtol(argv[2], &end, 10);
    if (*end != '\0') {
      fprintf(stderr, "Error while parsing plug count argument (int is required)\n");
      exit(1);
    }
  }

  if (argc > 3) {
    polling_period = strtol(argv[3], &end, 10);
    if (*end != '\0') {
      fprintf(stderr, "Error while parsing polling period argument (int is required)\n");
      exit(1);
    }
  }

  send_configuration_frame(fd, polling_period);
  read_device_values(fd, plug_count);

  return 0;
}
