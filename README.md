# Wattmetre-read

Read values from OmagaWatt Multilab-R electrical power monitoring devices


## Compilation

  gcc -std=c11 -lm -D DEBUG=1 -o wattmetre-read wattmetre_read.c

`-D DEBUG=1` is optional to enable debug mode where frames content are displayed

## Usage

  ./wattmetre-read [tty-device] [plug-count] [polling-period-in-ms]

Example:

  ./wattmetre-read /dev/ttyUSB0 42 20

## Output

Prints one line for each frame received (every [polling-period] milliseconds). Each line has several fields separated by comma (CSV format):

- If DEBUG is enabled, raw and escaped frame content.
- Timestamp when the measure was performed (as number of seconds and nano-seconds since 00:00:00 1970-01-01 UTC).
- Must be "OK" if the measure has correctly been performed (currently only check frame length)
- Following fields: Electrical power consumed for each wattmetre's port. Sometimes the value may be missing for a particular port. It means that wattmetre was not able to compute it correctly.
